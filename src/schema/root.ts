import { gql, IResolvers, ITypeDefinitions } from 'apollo-server-express'
import GraphQLJSON from 'graphql-type-json'
// import { merge } from 'lodash'

// import { combineTypeDefs } from '../utils'

export const RootQuery = gql`
  scalar JSON

  type MutationResponse {
    code: Int!
    message: String!
    status: String!
  }

  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  schema {
    query: Query
    mutation: Mutation
  }
`

const TypeDefs: ITypeDefinitions = () => [RootQuery]

const Resolvers: IResolvers = {
  Query: {},
  Mutation: {},
  JSON: GraphQLJSON,
}

export default { Resolvers, TypeDefs }
