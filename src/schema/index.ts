import { IResolvers } from 'apollo-server-express'
import { DocumentNode } from 'graphql'
import { merge } from 'lodash'

import Components from './components'
import Root from './root'

import { combineTypeDefs } from '../utils'

export const resolvers: IResolvers = merge(Components.Resolvers, Root.Resolvers)

export const typeDefs: DocumentNode[] = combineTypeDefs([Components.TypeDefs, Root.TypeDefs])
