import Resolvers from './resolvers'
import TypeDefs from './typedefs'

export default { Resolvers, TypeDefs }
