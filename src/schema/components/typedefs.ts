import { gql, ITypeDefinitions } from 'apollo-server-express'

export const Components = gql`
  """
  Resolves the general configuration settings from StoryBlok
  """
  type Config {
    resolvedConfig: Boolean!
    useRecaptcha: Boolean!
  }

  extend type Query {
    getComponent(component: String!, spaceID: String!): JSON!
    getComponents(spaceID: String!): JSON!
  }

  extend type Mutation {
    createComponent(component: String!, spaceID: String!): MutationResponse!
  }
`

const TypeDefs: ITypeDefinitions = () => [Components]

export default TypeDefs
