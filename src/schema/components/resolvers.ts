import { IResolvers } from 'apollo-server-express'

import { createComponent, getComponent, getAllComponents } from '../../models/components'
import { Context } from '../../types'

const Resolvers: IResolvers = {
  Query: {
    getComponent: (_, { component, spaceID }, { storyblokAPI }: Context) =>
      getComponent(storyblokAPI, component, spaceID),
    getComponents: (_, { spaceID }, { storyblokAPI }: Context) => getAllComponents(storyblokAPI, spaceID),
  },
  Mutation: {
    createComponent: (_, { component, spaceID }, { storyblokAPI }) => createComponent(storyblokAPI, component, spaceID),
  },
}

export default Resolvers
