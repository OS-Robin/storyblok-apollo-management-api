/* eslint-disable @typescript-eslint/no-explicit-any */
import { ApolloError } from 'apollo-server-express'

// eslint-disable-next-line no-shadow
export enum ErrorMessages {
  'Invalid_Origin' = 'REQ:443',
  'Invalid_Token_Or_Authorization' = 'Req:444',
  'No_Allowed_Queries' = 'Req:445',
  'JWT_Expired' = 'REQ:446',
  'JWS_invalid' = 'REQ:447',
  'Invalid_Request_Type' = 'REQ:448',
  'Missing_Credentials' = 'REQ:449',
  'Internal_Server_Error' = 'REQ:500',
}

type ErrorSource = '[STORYBLOK-API]'
type ErrorSourceKeys = 'storyblok'

export const errorSources: Record<ErrorSourceKeys, ErrorSource> = {
  storyblok: '[STORYBLOK-API]',
}

export const parseError = (error: any, extensions: any, code?: string): Error => {
  const errorMessage: string = error || 'Something unexpected went wrong'
  const errorCode: string = code || '500'

  throw new ApolloError(errorMessage, errorCode, extensions)
}
