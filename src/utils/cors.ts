import { ApolloError } from 'apollo-server-express'
import * as cors from 'cors'

import { ErrorMessages } from './error'

export const corsOptions: cors.CorsOptions = {
  origin: (origin, callback) => {
    const whitelist = [process.env.PUBLIC_URL]

    const originWithoutPort: string = (origin || '').split(':').slice(0, 2).join(':')

    if (process.env.NODE_ENV === 'development' && originWithoutPort === 'http://localhost') {
      return callback(null, true)
    }

    if (whitelist.indexOf(origin) !== -1) {
      return callback(null, true)
    }

    return callback(new ApolloError(ErrorMessages.Invalid_Origin, '401'))
  },
  credentials: true,
}
