import { ApolloError } from 'apollo-server-express'

import { ErrorMessages } from './error'
import StoryblokAPI from '../services/storyblok/api'
import { Context } from '../types'

export const resolveContext = async (req: any): Promise<Context> => {
  const { authorization } = req.headers

  if (authorization !== process.env.ACCESS_TOKEN) {
    throw new ApolloError(ErrorMessages.Invalid_Token_Or_Authorization, '401')
  }

  const storyblokAPI = await StoryblokAPI()

  return { storyblokAPI }
}
