import { gql } from 'apollo-server-express'

const parseQuery = (query: string) => {
  const obj = gql`
    ${query}
  ` as any

  return obj.definitions[0]?.selectionSet.selections[0].name.value
}

export { parseQuery }
