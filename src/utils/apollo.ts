import { ITypeDefinitions } from 'apollo-server-express'
import { DocumentNode } from 'graphql'
import { isFunction } from 'lodash'

export const combineTypeDefs = (items: ITypeDefinitions[]) => {
  return items.reduce((combinedTypeDefs: [], item: DocumentNode) => {
    const typeDefs = isFunction(item) ? item() : item

    return Array.isArray(typeDefs) ? [...combinedTypeDefs, ...typeDefs] : [...combinedTypeDefs, typeDefs]
  }, [])
}
