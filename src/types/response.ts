export interface MutationResponse {
  code: number
  message: string
  status: string
}
