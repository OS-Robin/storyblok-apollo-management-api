import { AxiosInstance } from 'axios'

export interface Context {
  storyblokAPI: AxiosInstance
}
