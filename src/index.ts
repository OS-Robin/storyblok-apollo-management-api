import { ApolloServer } from 'apollo-server-express'
import * as dotenv from 'dotenv'
import * as express from 'express'

dotenv.config()

// eslint-disable-next-line import/first
import { resolvers, typeDefs } from './schema'
import { resolveContext } from './utils'

export const server = new ApolloServer({
  context: ({ req }) => resolveContext(req),
  resolvers,
  typeDefs,
})

export const configureServer = () => {
  const app = express()

  server.applyMiddleware({ app, path: '/' })

  return app
}

const app = configureServer()
const port = 4000

// eslint-disable-next-line no-console
app.listen({ port }, () => console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`))
