import axios from 'axios'

type StoryBlokLink = 'story' | 'url'

interface StoryblokSpace {
  id: number
  name: string
  domain: string
  version: number
  languageCodes: string[]
}

interface StoryblokVersionResponse {
  space: StoryblokSpace
}

interface StoryblokParams {
  ev: number
  // eslint-disable-next-line camelcase
  resolve_links: string
  token: string
  version?: string
}

// interface StoryblokApiConfig {
//   enableDraftMode?: boolean
//   resolveLinks?: StoryBlokLink
// }

export const getVersion = async (): Promise<StoryblokVersionResponse> => {
  const { data } = await axios.get(
    `${process.env.STORYBLOK_API_ENDPOINT}/spaces/me?token=${process.env.STORYBLOK_TOKEN}`,
  )

  return { ...data, languageCodes: data.language_codes }
}

export const getParams = (
  version: number,
  enableDraftMode: boolean,
  resolveLinks: StoryBlokLink = 'url',
): StoryblokParams => ({
  ev: version,
  resolve_links: resolveLinks,
  token: process.env.STORYBLOK_TOKEN,
  ...(enableDraftMode ? { version: 'draft' } : {}),
})

export default async function (
  // { enableDraftMode = false, resolveLinks = 'url' }: StoryblokApiConfig
  ){
  // const {
  //   space: { version },
  // } = await getVersion()

  // const params = getParams(version, enableDraftMode, resolveLinks)

  return axios.create({
    baseURL: process.env.STORYBLOK_API_ENDPOINT,
    headers: {
      Authorization: process.env.STORYBLOK_AUTH_TOKEN
    }
  })
}
