import { ApolloError } from 'apollo-server-express'
import { AxiosInstance } from 'axios'
import * as fs from 'fs'

import { MutationResponse } from '../../types'

const dir = './components'

export const getComponent = async (storyblokAPI: AxiosInstance, component: string, spaceID: string): Promise<any> => {
  try {
    const {
      data: { components },
    } = await storyblokAPI.get(`${spaceID}/components`)

    const storyBlokComponent = components.find(({ name }) => name === component)

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir)
    }

    fs.writeFileSync(`${dir}/${storyBlokComponent.name}.json`, JSON.stringify(storyBlokComponent))

    return component
  } catch (error) {
    console.log('error', error.response.data)

    throw new ApolloError('Failed resolving the `getComponent` function ')
  }
}

export const getAllComponents = async (storyblokAPI: AxiosInstance, spaceID: string): Promise<any[]> => {
  try {
    const {
      data: { components },
    } = await storyblokAPI.get(`${spaceID}/components`)

    return components
  } catch (error) {
    console.log('error', error.response.data)

    throw new ApolloError('Failed resolving the `getAllComponents` function ')
  }
}

export const createComponent = async (
  storyblokAPI: AxiosInstance,
  component: string,
  spaceID: string,
): Promise<MutationResponse> => {
  try {
    const storyBlokComponent = JSON.parse(fs.readFileSync(`${dir}/${component}.json`, 'utf-8'))

    await storyblokAPI.post(`${spaceID}/components`, storyBlokComponent)

    return {
      code: 200,
      message: `Created new component: ${component}`,
      status: '',
    }
  } catch (error) {
    const errorResponse = error.response.data

    if (errorResponse?.name) {
      return {
        code: 409,
        message: `name: ${errorResponse.name[0]}: ${component}`,
        status: 'Failed',
      }
    }

    console.log('error', error.response.data)

    return {
      code: 500,
      message: `Failed to create new component: ${component}`,
      status: 'Failed',
    }
  }
}
