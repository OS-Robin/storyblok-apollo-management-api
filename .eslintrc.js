module.exports = {
  env: {
    jest: true,
  },
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  // extends: ['airbnb-typescript', 'airbnb-typescript-prettier'],
  plugins: ['@typescript-eslint', 'eslint-plugin-import'],
  parserOptions: {
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true, // Allows for the parsing of JSX
    },
  },
  rules: {
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/explicit-member-accessibility': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/prefer-default-export': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'no-underscore-dangle': 'off',
    'no-use-before-define': [0],
    '@typescript-eslint/no-use-before-define': [1],
    'import/no-unresolved': ['error', { ignore: ['^@'] }],
    'jsx-a11y/anchor-is-valid': [
      'error',
      { components: ['Link'], specialLink: ['hrefLeft', 'hrefRight'], aspects: ['invalidHref', 'preferButton'] },
    ],
  },
}
